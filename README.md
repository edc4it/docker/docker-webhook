[Webhook](https://github.com/adnanh/webhook/) Dockerized
=================

This  [almir/webhook/](https://hub.docker.com/r/almir/webhook/) with curl installed. It is used on our Ansible courses for a simple trigger of a AWX Job in a CD pipeline of a playbook.

