FROM almir/webhook:2.6.8

RUN apk update \
     && apk add  curl \
     && rm -rf /var/cache/apk/*